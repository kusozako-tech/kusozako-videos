
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity

TO_RECENT_PATHS_MODEL = {
    "type": "switcher",
    "title": _("Recent"),
    "message": "delta > switch stack to",
    "user-data": "recent-paths",
    }

RECENT_PATHS_MODEL = {
    "page-name": "recent-paths",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "recent-paths",
            "display-type": "shorten",
            "message": "delta > path selected",
            "user-data": None,
            "query": "delta > application recent paths",
            "query-data": None,
            "close-on-clicked": True
        }
    ]
}


class DeltaRecentPaths(DeltaEntity):

    def receive_transmission(self, user_data):
        message, path = user_data
        if message == "_delta_call_path_selected":
            self._raise("delta > uri selected", Gio.File.new_for_path(path))

    def __init__(self, parent):
        self._parent = parent
        item_data = "main", TO_RECENT_PATHS_MODEL
        self._raise("delta > application popover add item", item_data)
        self._raise("delta > application popover add page", RECENT_PATHS_MODEL)
        self._raise("delta > register application object", self)
