
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from kusozako_videos.player.Player import DeltaPlayer
from .StartupUri import DeltaStartupUri
from .Action import DeltaAction
from .RecentPaths import DeltaRecentPaths


class DeltaFileHandler(DeltaEntity):

    def _delta_call_uri_selected(self, gio_file):
        self._transmitter.transmit(("uri", gio_file.get_uri()))
        self._raise("delta > application recent paths", gio_file.get_path())

    def _delta_call_file_has_video(self, has_video):
        self._transmitter.transmit(("has-video", has_video))

    def _delta_call_register_uri_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_playbin_initialized(self):
        DeltaStartupUri(self)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaAction(self)
        DeltaRecentPaths(self)
        DeltaPlayer(self)
