
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser

MODEL = {
    "type": "simple-action",
    "title": _("Open"),
    "message": "delta > action",
    "user-data": "file.open",
    "close-on-clicked": True,
    "shortcut": "Ctrl+O"
    }

FILE_CHOOSER_MODEL = {
    "type": "select-file",
    "title": _("Select Media File"),
    "filters": {"Movie Files": "video/*", "Music Files": "audio/*"}
    }


class DeltaAction(DeltaEntity):

    def receive_transmission(self, action_id):
        if action_id != "file.open":
            return
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is not None:
            gio_file = Gio.File.new_for_path(path)
            self._raise("delta > uri selected", gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
        self._raise("delta > application popover add item", ("main", MODEL))
