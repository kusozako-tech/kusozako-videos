
from libkusozako3.Entity import DeltaEntity


class DeltaBus(DeltaEntity):

    @classmethod
    def new_for_playbin(cls, parent, playbin, handle):
        bus = cls(parent)
        bus.set_playbin(playbin, handle)

    def _on_state_changed(self, gst_bus, gst_message, playbin):
        _, new_state, _ = gst_message.parse_state_changed()
        self._raise("delta > state changed", new_state)
        has_video = playbin.get_property("n-video") > 0
        self._raise("delta > file has video", has_video)

    def _on_eos(self, *args):
        self._raise("delta > stream finished")

    def _on_sync_message(self, bus, message, handle):
        structure = message.get_structure()
        message_name = structure.get_name()
        if message_name != "prepare-window-handle":
            return
        play_sink = message.src
        play_sink.set_property("force-aspect-ratio", True)
        play_sink.set_window_handle(handle)

    def set_playbin(self, playbin, handle):
        bus = playbin.get_bus()
        bus.enable_sync_message_emission()
        bus.connect("sync-message::element", self._on_sync_message, handle)
        bus.add_signal_watch()
        bus.connect("message::eos", self._on_eos)
        bus.connect('message::state-changed', self._on_state_changed, playbin)

    def __init__(self, parent):
        self._parent = parent
