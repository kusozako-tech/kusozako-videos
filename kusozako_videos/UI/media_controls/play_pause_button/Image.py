
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity

SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaImage(Gtk.Image, DeltaEntity):

    def _set_to_pause(self):
        self.set_from_icon_name("media-playback-pause-symbolic", SIZE)
        self._raise("delta > message value changed", Gst.State.PAUSED)
        self.props.tooltip_text = _("Pause")

    def _set_to_play(self):
        self.set_from_icon_name("media-playback-start-symbolic", SIZE)
        self._raise("delta > message value changed", Gst.State.PLAYING)
        self.props.tooltip_text = _("Play")

    def _reset(self, value):
        if value == Gst.State.PLAYING:
            self._set_to_pause()
        else:
            self._set_to_play()

    def receive_transmission(self, user_data):
        type_, value = user_data
        if type_ == "state-changed":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self,icon_name="media-playback-pause-symbolic")
        self._raise("delta > register player object", self)
