
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .EventControllerMotion import DeltaEventControllerMotion
from .GestureSingle import DeltaGestureSingle

BUTTON_PRESS = Gdk.EventMask.BUTTON_PRESS_MASK
POINTER_MOTION = Gdk.EventMask.POINTER_MOTION_MASK
LEAVE_NOTIFY = Gdk.EventMask.LEAVE_NOTIFY_MASK
EVENTS = BUTTON_PRESS | POINTER_MOTION | LEAVE_NOTIFY


class DeltaEventControllers(DeltaEntity):

    def _delta_call_pointer_position(self, position=None):
        self._pointer_position = position
        self._raise("delta > queue draw")

    def __init__(self, parent):
        self._parent = parent
        self._pointer_position = None
        drawing_area = self._enquiry("delta > drawing area")
        drawing_area.add_events(EVENTS)
        self._motion = DeltaEventControllerMotion(self)
        self._gesture = DeltaGestureSingle(self)

    @property
    def pointer_position(self):
        return self._pointer_position
