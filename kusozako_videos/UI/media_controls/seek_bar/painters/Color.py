
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity

COLOR_QUERY = "css", "highlight_bg_color", "rgba(96, 114, 129, 1)"


class DeltaColor(DeltaEntity):

    def _reset(self):
        rgba = Gdk.RGBA()
        rgba.parse(self._enquiry("delta > settings", COLOR_QUERY))
        self._rgba = (rgba.red, rgba.green, rgba.blue, rgba.alpha)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "css":
            self._reset()

    def __init__(self, parent):
        self._parent = parent
        self._reset()
        self._raise("delta > register settings object", self)

    @property
    def rgba(self):
        return self._rgba
