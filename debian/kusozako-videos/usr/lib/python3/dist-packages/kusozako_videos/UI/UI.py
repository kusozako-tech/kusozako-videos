
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .viewers.Viewers import DeltaViewers
from .media_controls.MediaControls import DeltaMediaControls


class DeltaUI(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaViewers(self)
        DeltaMediaControls(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "background"))
