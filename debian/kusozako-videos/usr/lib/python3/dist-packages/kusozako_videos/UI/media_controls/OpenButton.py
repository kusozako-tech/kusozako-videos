
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity

SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaOpenButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > action", "file.open")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name("folder-open-symbolic", SIZE),
            tooltip_text=_("Open Media File")
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
