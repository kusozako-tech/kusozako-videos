
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Thumbnail import DeltaThumbnail


class DeltaCoverArt(Gtk.DrawingArea, DeltaEntity):

    def _get_x_position(self, rectangle, pixbuf):
        if self._enquiry("delta > is vertically long"):
            return (rectangle.width-pixbuf.get_width())/2
        return rectangle.width - (pixbuf.get_width())

    def _on_draw(self, drawing_area, cairo_context):
        self._raise("delta > check resized")
        rectangle, _ = drawing_area.get_allocated_size()
        pixbuf = self._thumbnail.get_pixbuf()
        if pixbuf is None:
            return
        Gdk.cairo_set_source_pixbuf(
            cairo_context,
            pixbuf,
            self._get_x_position(rectangle, pixbuf),
            (rectangle.height-pixbuf.get_height())/2,
            )
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True)
        self.set_size_request(-1, Unit(24))
        self._thumbnail = DeltaThumbnail(self)
        self._raise("delta > add to container", self)
        self.connect("draw", self._on_draw)
