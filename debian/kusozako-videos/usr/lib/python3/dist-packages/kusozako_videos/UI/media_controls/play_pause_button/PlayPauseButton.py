
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Image import DeltaImage


class DeltaPlayPauseButton(Gtk.Button, DeltaEntity):

    def _delta_call_message_value_changed(self, message_value):
        self._message_value = message_value
        self.set_sensitive(True)

    def _on_clicked(self, button):
        self._raise("delta > player state", self._message_value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=DeltaImage(self)
            )
        self.set_sensitive(False)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
