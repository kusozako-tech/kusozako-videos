
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODEL = [
    {
        "page-name": "main",
        "items": [
            {
                "type": "audio-volume",
                "maximum": 1,
                "minimum": 0,
                "round": 2,
                "message": "delta > settings",
                "user-data": ("player", "software_volume"),
                "query": "delta > settings",
                "query-data": ("player", "software_volume", 0.9)
            }
        ]
    }
]
