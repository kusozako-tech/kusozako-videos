
from libkusozako3.Entity import DeltaEntity


class DeltaTimings(DeltaEntity):

    def receive_transmission(self, user_data):
        type_, data = user_data
        if type_ == "position-updated":
            self._current = data
            self._raise("delta > queue draw")

    def __init__(self, parent):
        self._parent = parent
        self._current = 0, 1
        self._raise("delta > register player object", self)

    @property
    def timing(self):
        return self._current
